/*
   SPDX-FileCopyrightText: 2013-2022 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: GPL-2.0-or-later
*/
#pragma once

#include <QString>

class ThemeEditorUtil
{
public:
    ThemeEditorUtil();
    Q_REQUIRED_RESULT QString defaultMail() const;
};
